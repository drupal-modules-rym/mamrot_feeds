<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function mamrot_feeds_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => 1);
  }
  elseif ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => 2);
  }
}

/**
 * Implementation of hook_node_info().
 */
function mamrot_feeds_node_info() {
  $items = array(
    'mamrot_feed' => array(
      'name' => t('mamrot feed'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'has_body' => '1',
      'body_label' => t('Corps'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'mamrot_mrcs_feed' => array(
      'name' => t('mamrot mrcs feed'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'has_body' => '1',
      'body_label' => t('Corps'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'mamrot_muni_feed' => array(
      'name' => t('mamrot muni feed'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'has_body' => '1',
      'body_label' => t('Corps'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'mrc' => array(
      'name' => t('MRC'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'has_body' => '1',
      'body_label' => t('Corps'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'mrc_feed' => array(
      'name' => t('MRC feed'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'has_body' => '1',
      'body_label' => t('Corps'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'muni' => array(
      'name' => t('Municipalité'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'has_body' => '1',
      'body_label' => t('Corps'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function mamrot_feeds_views_api() {
  return array(
    'api' => '2',
  );
}

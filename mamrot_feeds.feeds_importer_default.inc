<?php

/**
 * Implementation of hook_feeds_importer_default().
 */
function mamrot_feeds_feeds_importer_default() {
  $export = array();
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mamrot_main';
  $feeds_importer->config = array(
    'name' => 'mamrot main',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => 'td[3]/a',
          'xpathparser:1' => 'td[3]/a/@href',
        ),
        'rawXML' => array(
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
        ),
        'context' => '//tbody/tr',
        'exp' => array(
          'errors' => 0,
          'tidy' => 0,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsFeedNodeProcessor',
      'config' => array(
        'content_type' => 'mamrot_muni_feed',
        'update_existing' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:1',
            'target' => 'source',
            'unique' => 1,
          ),
        ),
      ),
    ),
    'content_type' => 'mamrot_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );

  $export['mamrot_main'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mamrot_mrcs';
  $feeds_importer->config = array(
    'name' => 'mamrot mrcs',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => '.',
          'xpathparser:1' => './@value',
        ),
        'rawXML' => array(
          'xpathparser:0' => 'xpathparser:0',
          'xpathparser:1' => 'xpathparser:1',
        ),
        'context' => '//select[@id=\'mrc-cm-arg\']/option',
        'exp' => array(
          'errors' => 0,
          'tidy' => 1,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsFeedNodeProcessor',
      'config' => array(
        'content_type' => 'mrc_feed',
        'update_existing' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:1',
            'target' => 'source',
            'unique' => 1,
          ),
        ),
      ),
    ),
    'content_type' => 'mamrot_mrcs_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
  );

  $export['mamrot_mrcs'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mamrot_muni';
  $feeds_importer->config = array(
    'name' => 'mamrot_muni',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:1' => '//h2[@class=\'margin-bottom\']',
          'xpathparser:2' => '//div[@id=\'onglet-information\']/ul[1]/li[1]',
          'xpathparser:3' => '//div[@id=\'onglet-information\']/ul[1]/li[3]',
          'xpathparser:4' => '//div[@id=\'onglet-information\']/ul[2]/li[3]',
          'xpathparser:5' => '//div[@id=\'onglet-information\']/ul[2]/li[4]',
          'xpathparser:6' => '//div[@id=\'onglet-information\']/ul[2]/li[5]',
          'xpathparser:7' => '//div[@id=\'onglet-information\']/ul[3]/li[1]',
          'xpathparser:8' => '//div[@id=\'onglet-information\']/ul[3]/li[2]',
          'xpathparser:9' => '//div[@id=\'onglet-information\']/ul[3]/li[3]',
          'xpathparser:10' => '//div[@id=\'onglet-information\']/ul[3]/li[4]',
          'xpathparser:11' => '//div[@id=\'onglet-information\']/ul[3]/li[5]',
          'xpathparser:12' => '//div[@id=\'onglet-information\']/ul[3]/li[6]',
          'xpathparser:13' => '//div[@id=\'onglet-information\']/ul[3]/li[7]',
          'xpathparser:14' => '//div[@id=\'onglet-information\']/ul[1]/li[1]',
        ),
        'rawXML' => array(
          'xpathparser:1' => 0,
          'xpathparser:2' => 0,
          'xpathparser:3' => 0,
          'xpathparser:4' => 0,
          'xpathparser:5' => 0,
          'xpathparser:6' => 0,
          'xpathparser:7' => 0,
          'xpathparser:8' => 0,
          'xpathparser:9' => 0,
          'xpathparser:10' => 0,
          'xpathparser:11' => 0,
          'xpathparser:12' => 0,
          'xpathparser:13' => 0,
          'xpathparser:14' => 0,
        ),
        'context' => '//div[@id=\'wrap-droite\']',
        'exp' => array(
          'errors' => 0,
          'tidy' => 1,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'context' => 0,
            'xpathparser:1' => 0,
            'xpathparser:2' => 0,
            'xpathparser:3' => 0,
            'xpathparser:4' => 0,
            'xpathparser:5' => 0,
            'xpathparser:6' => 0,
            'xpathparser:7' => 0,
            'xpathparser:8' => 0,
            'xpathparser:9' => 0,
            'xpathparser:10' => 0,
            'xpathparser:11' => 0,
            'xpathparser:12' => 0,
            'xpathparser:13' => 0,
            'xpathparser:14' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'muni',
        'input_format' => '0',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:1',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:2',
            'target' => 'field_statcan',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'xpathparser:3',
            'target' => 'field_gentile',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:4',
            'target' => 'field_recensement_canada',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_communaute_metropolitaire',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:6',
            'target' => 'field_election_provinciale',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:7',
            'target' => 'field_superficie',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'xpathparser:8',
            'target' => 'field_population',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'xpathparser:9',
            'target' => 'field_date_constitution',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'xpathparser:10',
            'target' => 'field_date_changement_regime',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'xpathparser:11',
            'target' => 'field_date_prochaine_election',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'xpathparser:12',
            'target' => 'field_mode_election',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'xpathparser:13',
            'target' => 'field_division_territoriale',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'xpathparser:14',
            'target' => 'guid',
            'unique' => 1,
          ),
        ),
        'author' => '1',
      ),
    ),
    'content_type' => 'mamrot_muni_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
  );

  $export['mamrot_muni'] = $feeds_importer;
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mrc_feed';
  $feeds_importer->config = array(
    'name' => 'mrc feed',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserHTML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => '//h2[@class="margin-bottom"]',
          'xpathparser:1' => '//div[@id="onglet-organisation"]/ul[1]/li[1]',
          'xpathparser:2' => '//div[@id="onglet-organisation"]/ul[1]/li[2]',
          'xpathparser:5' => '//div[@id="onglet-organisation"]/ul[2]/li',
          'xpathparser:6' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:7' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:8' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:9' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p',
          'xpathparser:10' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p',
          'xpathparser:11' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[@class=\'liens\']/a[2]/@href',
          'xpathparser:12' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[@class=\'liens\']/a[1]/@href',
          'xpathparser:13' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[4]',
          'xpathparser:14' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[4]',
          'xpathparser:15' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[4]',
          'xpathparser:16' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:17' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[2]',
          'xpathparser:18' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[4]',
          'xpathparser:19' => '//div[@id=\'c503\']/div[@class=\'repertoire\']/div[@class=\'dans-cette-page\']/p[4]',
          'xpathparser:21' => '//div[@id="onglet-organisation"]/ul[1]/li[5]',
          'xpathparser:22' => '//div[@id="onglet-organisation"]/ul[1]/li[1]',
        ),
        'rawXML' => array(
          'xpathparser:6' => 'xpathparser:6',
          'xpathparser:7' => 'xpathparser:7',
          'xpathparser:8' => 'xpathparser:8',
          'xpathparser:9' => 'xpathparser:9',
          'xpathparser:10' => 'xpathparser:10',
          'xpathparser:13' => 'xpathparser:13',
          'xpathparser:14' => 'xpathparser:14',
          'xpathparser:15' => 'xpathparser:15',
          'xpathparser:16' => 'xpathparser:16',
          'xpathparser:17' => 'xpathparser:17',
          'xpathparser:18' => 'xpathparser:18',
          'xpathparser:19' => 'xpathparser:19',
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
          'xpathparser:2' => 0,
          'xpathparser:5' => 0,
          'xpathparser:11' => 0,
          'xpathparser:12' => 0,
          'xpathparser:21' => 0,
          'xpathparser:22' => 0,
        ),
        'context' => '//div[@id=\'c11\']/div',
        'exp' => array(
          'errors' => 0,
          'tidy' => 1,
          'tidy_encoding' => 'UTF8',
          'debug' => array(
            'xpathparser:1' => 'xpathparser:1',
            'xpathparser:21' => 'xpathparser:21',
            'xpathparser:22' => 'xpathparser:22',
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:2' => 0,
            'xpathparser:5' => 0,
            'xpathparser:6' => 0,
            'xpathparser:7' => 0,
            'xpathparser:8' => 0,
            'xpathparser:9' => 0,
            'xpathparser:10' => 0,
            'xpathparser:11' => 0,
            'xpathparser:12' => 0,
            'xpathparser:13' => 0,
            'xpathparser:14' => 0,
            'xpathparser:15' => 0,
            'xpathparser:16' => 0,
            'xpathparser:17' => 0,
            'xpathparser:18' => 0,
            'xpathparser:19' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'mrc',
        'input_format' => '0',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'xpathparser:1',
            'target' => 'guid',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'xpathparser:2',
            'target' => 'taxonomy:1',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'xpathparser:5',
            'target' => 'field_direction',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'xpathparser:6',
            'target' => 'field_location:street',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'xpathparser:7',
            'target' => 'field_location:city',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'xpathparser:8',
            'target' => 'field_location:postal_code',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'xpathparser:9',
            'target' => 'field_location:fax',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'xpathparser:10',
            'target' => 'field_location:phone',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'xpathparser:11',
            'target' => 'field_siteweb:url',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'xpathparser:12',
            'target' => 'field_email',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'xpathparser:13',
            'target' => 'field_postal:street',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'xpathparser:14',
            'target' => 'field_postal:city',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'xpathparser:15',
            'target' => 'field_postal:postal_code',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'xpathparser:16',
            'target' => 'field_location:province',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'xpathparser:17',
            'target' => 'field_location:country',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'xpathparser:18',
            'target' => 'field_postal:province',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'xpathparser:19',
            'target' => 'field_postal:country',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'xpathparser:21',
            'target' => 'field_date_constitution:start',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'xpathparser:22',
            'target' => 'field_geocode',
            'unique' => FALSE,
          ),
        ),
        'author' => 0,
      ),
    ),
    'content_type' => 'mrc_feed',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
  );

  $export['mrc_feed'] = $feeds_importer;
  return $export;
}

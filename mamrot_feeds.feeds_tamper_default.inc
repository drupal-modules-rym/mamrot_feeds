<?php

/**
 * Implementation of hook_feeds_tamper_default().
 */
function mamrot_feeds_feeds_tamper_default() {
  $export = array();
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'absolute_mamrot';
  $feeds_tamper->importer = 'mamrot_main';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'repertoire-des-municipalites/fiche/municipalite/',
    'replace' => 'http://www.mamrot.gouv.qc.ca/repertoire-des-municipalites/fiche/municipalite/',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'faire lien absolus de mamrot';

  $export['absolute_mamrot'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'codegeo0';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:20';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '|^.*([0-9]+)$|U',
    'replace' => '$1',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['codegeo0'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'codepostal';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:15';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|<p>.*<br ?/>.?([a-z][0-9][a-z] ?[0-9][a-z][0-9])</p>|sUi',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['codepostal'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'codepostal1';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:8';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|<p>.*<br ?/>.?([a-z][0-9][a-z] ?[0-9][a-z][0-9])</p>|sUi',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['codepostal1'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'dateconst0';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:4';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '|^.*([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])$|U',
    'replace' => '$1',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['dateconst0'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'dateconst2';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:21';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^.*([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])$|U',
    'replace' => '$1',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['dateconst2'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'fax';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:9';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^.*copieur.*([0-9][0-9][0-9] [0-9][0-9][0-9]-[0-9][0-9][0-9][0-9])</p>.*$|Us',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['fax'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'fg';
  $feeds_tamper->importer = 'mamrot_mrcs';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '| value="-?([0-9]+)"|',
    'replace' => 'http://www.mamrot.gouv.qc.ca/repertoire-des-municipalites/fiche/mrc/$1/',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['fg'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'geocode2';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:22';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^.*([0-9]+)$|U',
    'replace' => '$1',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['geocode2'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'guid0';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:1';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '|^.*([0-9]+)$|U',
    'replace' => '$1',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['guid0'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'impfax';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:10';
  $feeds_tamper->plugin_id = 'implode';
  $feeds_tamper->settings = array(
    'glue' => '',
    'real_glue' => '',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Convert this multi-valued item to a single valued item separated by: "".';

  $export['impfax'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'impphone';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:9';
  $feeds_tamper->plugin_id = 'implode';
  $feeds_tamper->settings = array(
    'glue' => '',
    'real_glue' => '',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Convert this multi-valued item to a single valued item separated by: "".';

  $export['impphone'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'mailto0';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:12';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => 'mailto:',
    'replace' => '',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['mailto0'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'mamrot_mrcs_xpathparser_0_strip_html_tags';
  $feeds_tamper->importer = 'mamrot_mrcs';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Strip HTML tags.';

  $export['mamrot_mrcs_xpathparser_0_strip_html_tags'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'nocodepostal';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:15';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^<p.*$|s',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = '';

  $export['nocodepostal'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'nocodepostal1';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:8';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^<p>.*|s',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['nocodepostal1'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'nonum';
  $feeds_tamper->importer = 'mamrot_mrcs';
  $feeds_tamper->source = 'xpathparser:0';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^[0-9]+ (.*)$|',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = '';

  $export['nonum'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'norue';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:13';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^<p.*$|s',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = '';

  $export['norue'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'noville';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:14';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^<p.*$|s',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = '';

  $export['noville'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'pays';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:17';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/.*/s',
    'replace' => 'Canada',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['pays'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'pays0';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:19';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^<p>.*</p>$|s',
    'replace' => 'Canada',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['pays0'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'province';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:16';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/.*/s',
    'replace' => 'Quebec',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['province'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'province0';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:18';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '|^<p>.*</p>$|s',
    'replace' => 'Quebec',
    'limit' => '1',
    'real_limit' => '1',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['province0'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rue';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:13';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|<p>(.*)<br ?/>.*</p>|sU',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['rue'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rue1';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:6';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|<p>(.*)<br ?/>.*</p>|sU',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['rue1'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'tel';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:10';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|^.*phone.*([0-9][0-9][0-9] [0-9][0-9][0-9]-[0-9][0-9][0-9][0-9])<br.*$|su',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['tel'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'trimguidjunk';
  $feeds_tamper->importer = 'mamrot_muni';
  $feeds_tamper->source = 'xpathparser:14';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'ltrim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Remove "white space" from the left side.';

  $export['trimguidjunk'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'ville';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:14';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|<p>.*<br ?/>\\n(.*) \\(.*</p>|sU',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['ville'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'ville1';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:7';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => '|<p>.*<br ?/>\\n(.*) \\(.*</p>|sU',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['ville1'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'virgule';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:13';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => ', ',
    'replace' => ' ',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = '';

  $export['virgule'] = $feeds_tamper;
  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'virgule1';
  $feeds_tamper->importer = 'mrc_feed';
  $feeds_tamper->source = 'xpathparser:6';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'description' => '',
    'find' => ', ',
    'replace' => ' ',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 0,
    'regex' => FALSE,
    'func' => 'str_ireplace',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = '';

  $export['virgule1'] = $feeds_tamper;
  return $export;
}

<?php

/**
 * Implementation of hook_user_default_roles().
 */
function mamrot_feeds_user_default_roles() {
  $roles = array();

  // Exported role: conseiller
  $roles['conseiller'] = array(
    'name' => 'conseiller',
  );

  // Exported role: employé
  $roles['employé'] = array(
    'name' => 'employé',
  );

  // Exported role: maire
  $roles['maire'] = array(
    'name' => 'maire',
  );

  return $roles;
}
